﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GamesForCrypto.Model.Config
{
    class MiningData
    {
        public string WorkerId { get; set; }
        public long Hashrate { get; set; }
    }
}
