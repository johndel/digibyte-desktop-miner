﻿using GamesForCrypto.Core;
using GamesForCrypto.Core.Interfaces;
using GamesForCrypto.Model.Config;
using GamesForCrypto.View;
using GamesForCrypto.View.v1;
using GamesForCrypto.View.v1.ExtraScreens;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace GamesForCrypto
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }
        List<Form> m_Corousals = new List<Form>();
        int m_CurrentCarousal = 0;
        DateTime m_LastCarousalTurn = DateTime.Now;
        private const int CAROUSAL_WAIT=45;
        public MinerView MinerView { get; set; }//the selected minerview. not the activated one. this is the one which was clicked. 
        public MinerInfo MinerInfo { get; set; }//the selected minerInfo. not the activated one. this is the one which was clicked. We need this object to show the logs etc
        WebBrowserEx m_DownloaderBrowser = new WebBrowserEx();

        public WebBrowserEx DownloadBrowser
        {
            get
            {
                return m_DownloaderBrowser;
            }
        }

        private void btnAddMiner_Click(object sender, EventArgs e)
        {
            //Todo permission from core to add miner
            AddMinerContainer addMiner = new AddMinerContainer();
            addMiner.ShowDialog();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            this.FormClosing += MainForm_FormClosing;
            oneMinerNotifyIcon.ContextMenuStrip = taskbarMenu;
            Factory.Instance.ViewObject.RegisterForTimer(UpDateMinerState);

            oneMinerNotifyIcon.Click += oneMinerNotifyIcon_Click;
        }



        void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }


        public void UpdateMinerInfo()
        {
            if(MinerInfo!=null)
            {
                MinerInfo.UpdateUI();
            }
        }
        public void UpDateMinerState()
        {
            try
            {
                if(this.InvokeRequired)
                {
                    this.BeginInvoke(new Action(UpDateMinerState),
                                          new object[] {  });
                }
                else
                {
                    foreach (Control item in pnlMainInfo.Controls)
                    {
                        Home home = item as Home;
                        if (home != null)
                        {
                            home.UpdateState(false);
                        }
                        //Todo: this seems to be a duplicate call as timer invokes this separately. analyze
                        //MinerInfo.UpdateState();
                    }
                }
                
            }
            catch (Exception e)
            {
                Logger.Instance.LogError("Error while updating minerstate" + e.Message);
            }
         
        }


        protected override void WndProc(ref Message m)
        {
            if (m.Msg == 3000)
            {
                showToolStripMenuItem_Click(null, new EventArgs());
            }

            base.WndProc(ref m);
        }


        private Panel ClonePanel(Panel p)
        {
            return null;
        }
        public void SelectMiningView(IMiner miner)
        {
            ShowMiningInfo(miner);

        }

        public void UpdateMinerList()
        {
            IMiner miner = null;
            miner = Factory.Instance.CoreObject.SelectedMiner;
            Home view =null;
            if (pnlMainInfo.Controls.Count>0)
            {
                view = pnlMainInfo.Controls[0] as Home;
                view.Miner = miner;
                if(view!=null)
                {
                    view.UpdateState(true);
                }
            }
            else
            {
                view = new Home(miner, this);
                view.TopLevel = false;
                pnlMainInfo.Controls.Add(view);
                view.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                view.UpdateState(false);
                view.Show();
            }



            SelectMiningView(Factory.Instance.CoreObject.SelectedMiner);

        }


        public void ShowMiningInfo(IMiner miner)
        {
            MinerInfo info = new MinerInfo(miner, this);
            info.TopLevel = false;
            pnlMinerInfo.Controls.Clear();
            pnlMinerInfo.Controls.Add(info);
            info.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            info.Dock = DockStyle.Fill;
            this.MinerInfo = info;
            info.UpdateState();

            info.Show();
        }

        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
        }

        private void donateToolStripMenuItem1_Click(object sender, EventArgs e)
        {
        }

        private void pnlMinerInfo_Paint(object sender, PaintEventArgs e)
        {
        }

        private void advancedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings settings = new Settings();
            settings.ShowDialog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Factory.Instance.CoreObject.CloseApp();
        }

        private void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
        }


        private void donateToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }
        public void ShowHardwareMissingError()
        {
            MessageBox.Show("System could not detect any Nvidia or AMD graphics card in your machine! If this is by mistake, please select correct miners from Script tab ",
                "Hardware Missing");
        }

        private void modeToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void oneMinerNotifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
        }
        void oneMinerNotifyIcon_Click(object sender, EventArgs e)
        {

        }

        private void profitabilityToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void pnlMainInfo_Paint(object sender, PaintEventArgs e)
        {

        }



    }
}
