﻿namespace GamesForCrypto.View.v1
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Home));
            this.lblMinerState = new System.Windows.Forms.Label();
            this.btnStartMining = new System.Windows.Forms.Button();
            this.lblShares = new System.Windows.Forms.Label();
            this.lblTotalHashrate = new System.Windows.Forms.Label();
            this.lblMinername = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblAlgorithm = new System.Windows.Forms.Label();
            this.lblPool = new System.Windows.Forms.Label();
            this.lblWallet = new System.Windows.Forms.Label();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.lblSliderLocation1 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblSliderLocation2 = new System.Windows.Forms.Label();
            this.txtWorkerId = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.pnlStage1 = new System.Windows.Forms.Panel();
            this.linkLabel3 = new System.Windows.Forms.LinkLabel();
            this.label10 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblError = new System.Windows.Forms.Label();
            this.pnlStage2 = new System.Windows.Forms.Panel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pctHeroImage = new System.Windows.Forms.PictureBox();
            this.pctMiningState = new System.Windows.Forms.PictureBox();
            this.lblUsername = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.pnlStage1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlStage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctHeroImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctMiningState)).BeginInit();
            this.SuspendLayout();
            // 
            // lblMinerState
            // 
            this.lblMinerState.AutoSize = true;
            this.lblMinerState.BackColor = System.Drawing.Color.Transparent;
            this.lblMinerState.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMinerState.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblMinerState.Location = new System.Drawing.Point(1080, 246);
            this.lblMinerState.Name = "lblMinerState";
            this.lblMinerState.Size = new System.Drawing.Size(67, 20);
            this.lblMinerState.TabIndex = 33;
            this.lblMinerState.Text = "Stopped";
            this.lblMinerState.Visible = false;
            // 
            // btnStartMining
            // 
            this.btnStartMining.BackgroundImage = global::GamesForCrypto.Properties.Resources.start;
            this.btnStartMining.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartMining.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnStartMining.Location = new System.Drawing.Point(462, 483);
            this.btnStartMining.Name = "btnStartMining";
            this.btnStartMining.Size = new System.Drawing.Size(122, 53);
            this.btnStartMining.TabIndex = 32;
            this.btnStartMining.UseVisualStyleBackColor = true;
            this.btnStartMining.Click += new System.EventHandler(this.btnStartMining_Click_1);
            // 
            // lblShares
            // 
            this.lblShares.AutoSize = true;
            this.lblShares.BackColor = System.Drawing.Color.Transparent;
            this.lblShares.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShares.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblShares.Location = new System.Drawing.Point(1025, 369);
            this.lblShares.Name = "lblShares";
            this.lblShares.Size = new System.Drawing.Size(84, 13);
            this.lblShares.TabIndex = 31;
            this.lblShares.Text = "Shares: 0 S, 0 R";
            this.lblShares.Visible = false;
            // 
            // lblTotalHashrate
            // 
            this.lblTotalHashrate.AutoSize = true;
            this.lblTotalHashrate.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalHashrate.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalHashrate.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblTotalHashrate.Location = new System.Drawing.Point(1020, 314);
            this.lblTotalHashrate.Name = "lblTotalHashrate";
            this.lblTotalHashrate.Size = new System.Drawing.Size(154, 45);
            this.lblTotalHashrate.TabIndex = 30;
            this.lblTotalHashrate.Text = "Hashrate";
            this.lblTotalHashrate.Visible = false;
            // 
            // lblMinername
            // 
            this.lblMinername.AutoSize = true;
            this.lblMinername.BackColor = System.Drawing.Color.Transparent;
            this.lblMinername.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMinername.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblMinername.Location = new System.Drawing.Point(1088, 386);
            this.lblMinername.Name = "lblMinername";
            this.lblMinername.Size = new System.Drawing.Size(97, 20);
            this.lblMinername.TabIndex = 23;
            this.lblMinername.Text = "Defaultname";
            this.lblMinername.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(986, 414);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 17);
            this.label1.TabIndex = 35;
            this.label1.Text = "Algorithm";
            this.label1.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Location = new System.Drawing.Point(986, 458);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 17);
            this.label3.TabIndex = 37;
            this.label3.Text = "UserName";
            this.label3.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(986, 436);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 17);
            this.label4.TabIndex = 38;
            this.label4.Text = "Pool";
            this.label4.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Location = new System.Drawing.Point(1070, 414);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(11, 17);
            this.label5.TabIndex = 39;
            this.label5.Text = ":";
            this.label5.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label6.Location = new System.Drawing.Point(1070, 458);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(11, 17);
            this.label6.TabIndex = 40;
            this.label6.Text = ":";
            this.label6.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label7.Location = new System.Drawing.Point(1070, 436);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(11, 17);
            this.label7.TabIndex = 41;
            this.label7.Text = ":";
            this.label7.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label8.Location = new System.Drawing.Point(985, 386);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 20);
            this.label8.TabIndex = 42;
            this.label8.Text = "Profile";
            this.label8.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label9.Location = new System.Drawing.Point(1069, 386);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(13, 20);
            this.label9.TabIndex = 43;
            this.label9.Text = ":";
            this.label9.Visible = false;
            // 
            // lblAlgorithm
            // 
            this.lblAlgorithm.AutoSize = true;
            this.lblAlgorithm.BackColor = System.Drawing.Color.Transparent;
            this.lblAlgorithm.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlgorithm.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblAlgorithm.Location = new System.Drawing.Point(1089, 414);
            this.lblAlgorithm.Name = "lblAlgorithm";
            this.lblAlgorithm.Size = new System.Drawing.Size(85, 17);
            this.lblAlgorithm.TabIndex = 44;
            this.lblAlgorithm.Text = "Defaultname";
            this.lblAlgorithm.Visible = false;
            // 
            // lblPool
            // 
            this.lblPool.AutoSize = true;
            this.lblPool.BackColor = System.Drawing.Color.Transparent;
            this.lblPool.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPool.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblPool.Location = new System.Drawing.Point(1089, 436);
            this.lblPool.Name = "lblPool";
            this.lblPool.Size = new System.Drawing.Size(85, 17);
            this.lblPool.TabIndex = 45;
            this.lblPool.Text = "Defaultname";
            this.lblPool.Visible = false;
            // 
            // lblWallet
            // 
            this.lblWallet.AutoSize = true;
            this.lblWallet.BackColor = System.Drawing.Color.Transparent;
            this.lblWallet.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWallet.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblWallet.Location = new System.Drawing.Point(1089, 458);
            this.lblWallet.Name = "lblWallet";
            this.lblWallet.Size = new System.Drawing.Size(85, 17);
            this.lblWallet.TabIndex = 46;
            this.lblWallet.Text = "Defaultname";
            this.lblWallet.Visible = false;
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.linkLabel2.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.linkLabel2.LinkColor = System.Drawing.Color.Crimson;
            this.linkLabel2.Location = new System.Drawing.Point(1070, 278);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(28, 15);
            this.linkLabel2.TabIndex = 49;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "Edit";
            this.linkLabel2.Visible = false;
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // lblSliderLocation1
            // 
            this.lblSliderLocation1.AutoSize = true;
            this.lblSliderLocation1.BackColor = System.Drawing.Color.Transparent;
            this.lblSliderLocation1.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSliderLocation1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblSliderLocation1.Location = new System.Drawing.Point(460, 591);
            this.lblSliderLocation1.Name = "lblSliderLocation1";
            this.lblSliderLocation1.Size = new System.Drawing.Size(85, 17);
            this.lblSliderLocation1.TabIndex = 53;
            this.lblSliderLocation1.Text = "Defaultname";
            this.lblSliderLocation1.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label12.Location = new System.Drawing.Point(716, 591);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 17);
            this.label12.TabIndex = 51;
            this.label12.Text = "Intensity";
            // 
            // lblSliderLocation2
            // 
            this.lblSliderLocation2.AutoSize = true;
            this.lblSliderLocation2.BackColor = System.Drawing.Color.Transparent;
            this.lblSliderLocation2.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSliderLocation2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblSliderLocation2.Location = new System.Drawing.Point(460, 622);
            this.lblSliderLocation2.Name = "lblSliderLocation2";
            this.lblSliderLocation2.Size = new System.Drawing.Size(85, 17);
            this.lblSliderLocation2.TabIndex = 54;
            this.lblSliderLocation2.Text = "Defaultname";
            this.lblSliderLocation2.Visible = false;
            // 
            // txtWorkerId
            // 
            this.txtWorkerId.Location = new System.Drawing.Point(45, 304);
            this.txtWorkerId.Name = "txtWorkerId";
            this.txtWorkerId.Size = new System.Drawing.Size(276, 20);
            this.txtWorkerId.TabIndex = 56;
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::GamesForCrypto.Properties.Resources.button;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.Location = new System.Drawing.Point(45, 385);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(276, 49);
            this.button1.TabIndex = 57;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // pnlStage1
            // 
            this.pnlStage1.BackColor = System.Drawing.SystemColors.Highlight;
            this.pnlStage1.Controls.Add(this.linkLabel3);
            this.pnlStage1.Controls.Add(this.label10);
            this.pnlStage1.Controls.Add(this.panel1);
            this.pnlStage1.Location = new System.Drawing.Point(21, 588);
            this.pnlStage1.Name = "pnlStage1";
            this.pnlStage1.Size = new System.Drawing.Size(1373, 806);
            this.pnlStage1.TabIndex = 58;
            // 
            // linkLabel3
            // 
            this.linkLabel3.AutoSize = true;
            this.linkLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel3.ForeColor = System.Drawing.Color.SandyBrown;
            this.linkLabel3.LinkColor = System.Drawing.Color.Lavender;
            this.linkLabel3.Location = new System.Drawing.Point(648, 565);
            this.linkLabel3.Name = "linkLabel3";
            this.linkLabel3.Size = new System.Drawing.Size(63, 16);
            this.linkLabel3.TabIndex = 64;
            this.linkLabel3.TabStop = true;
            this.linkLabel3.Text = "Sign Up";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.DimGray;
            this.label10.Location = new System.Drawing.Point(470, 564);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(174, 17);
            this.label10.TabIndex = 63;
            this.label10.Text = "Don’t have an account yet?";
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::GamesForCrypto.Properties.Resources.frame;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.lblError);
            this.panel1.Controls.Add(this.txtWorkerId);
            this.panel1.Location = new System.Drawing.Point(441, 82);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(361, 461);
            this.panel1.TabIndex = 62;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label2.Location = new System.Drawing.Point(47, 256);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(274, 17);
            this.label2.TabIndex = 62;
            this.label2.Text = "Enter the user ID shown in your homepage.";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::GamesForCrypto.Properties.Resources.logo2;
            this.pictureBox1.Location = new System.Drawing.Point(101, 50);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(160, 161);
            this.pictureBox1.TabIndex = 62;
            this.pictureBox1.TabStop = false;
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.BackColor = System.Drawing.Color.Tomato;
            this.lblError.Location = new System.Drawing.Point(50, 345);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(0, 13);
            this.lblError.TabIndex = 58;
            // 
            // pnlStage2
            // 
            this.pnlStage2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pnlStage2.Controls.Add(this.pictureBox6);
            this.pnlStage2.Controls.Add(this.label13);
            this.pnlStage2.Controls.Add(this.label11);
            this.pnlStage2.Controls.Add(this.pictureBox5);
            this.pnlStage2.Controls.Add(this.pictureBox4);
            this.pnlStage2.Controls.Add(this.pctHeroImage);
            this.pnlStage2.Controls.Add(this.pctMiningState);
            this.pnlStage2.Controls.Add(this.lblUsername);
            this.pnlStage2.Controls.Add(this.linkLabel1);
            this.pnlStage2.Controls.Add(this.lblMinerState);
            this.pnlStage2.Controls.Add(this.label12);
            this.pnlStage2.Controls.Add(this.lblSliderLocation2);
            this.pnlStage2.Controls.Add(this.lblSliderLocation1);
            this.pnlStage2.Controls.Add(this.btnStartMining);
            this.pnlStage2.Location = new System.Drawing.Point(0, 0);
            this.pnlStage2.Name = "pnlStage2";
            this.pnlStage2.Size = new System.Drawing.Size(1373, 750);
            this.pnlStage2.TabIndex = 59;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::GamesForCrypto.Properties.Resources.u3;
            this.pictureBox6.Location = new System.Drawing.Point(1159, 23);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(48, 46);
            this.pictureBox6.TabIndex = 68;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Click += new System.EventHandler(this.pictureBox6_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label13.Location = new System.Drawing.Point(51, 638);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(223, 25);
            this.label13.TabIndex = 67;
            this.label13.Text = "www.games4crypto.com";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label11.Location = new System.Drawing.Point(84, 43);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(90, 25);
            this.label11.TabIndex = 66;
            this.label11.Text = "User ID : ";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::GamesForCrypto.Properties.Resources.u1;
            this.pictureBox5.Location = new System.Drawing.Point(30, 33);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(48, 46);
            this.pictureBox5.TabIndex = 65;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(471, 593);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(51, 30);
            this.pictureBox4.TabIndex = 64;
            this.pictureBox4.TabStop = false;
            // 
            // pctHeroImage
            // 
            this.pctHeroImage.BackColor = System.Drawing.Color.Transparent;
            this.pctHeroImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pctHeroImage.Image = global::GamesForCrypto.Properties.Resources.group_3;
            this.pctHeroImage.Location = new System.Drawing.Point(416, 23);
            this.pctHeroImage.Name = "pctHeroImage";
            this.pctHeroImage.Size = new System.Drawing.Size(443, 430);
            this.pctHeroImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctHeroImage.TabIndex = 63;
            this.pctHeroImage.TabStop = false;
            // 
            // pctMiningState
            // 
            this.pctMiningState.BackColor = System.Drawing.Color.Transparent;
            this.pctMiningState.Image = global::GamesForCrypto.Properties.Resources.stopped;
            this.pctMiningState.Location = new System.Drawing.Point(624, 483);
            this.pctMiningState.Name = "pctMiningState";
            this.pctMiningState.Size = new System.Drawing.Size(172, 53);
            this.pctMiningState.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctMiningState.TabIndex = 62;
            this.pctMiningState.TabStop = false;
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.BackColor = System.Drawing.Color.Transparent;
            this.lblUsername.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsername.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lblUsername.Location = new System.Drawing.Point(172, 43);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(119, 25);
            this.lblUsername.TabIndex = 61;
            this.lblUsername.Text = "Signed in as ";
            this.lblUsername.Click += new System.EventHandler(this.lblUsername_Click);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.linkLabel1.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.linkLabel1.LinkColor = System.Drawing.SystemColors.ActiveBorder;
            this.linkLabel1.Location = new System.Drawing.Point(1069, 33);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(77, 25);
            this.linkLabel1.TabIndex = 60;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Logout";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1453, 967);
            this.Controls.Add(this.pnlStage2);
            this.Controls.Add(this.pnlStage1);
            this.Controls.Add(this.linkLabel2);
            this.Controls.Add(this.lblWallet);
            this.Controls.Add(this.lblPool);
            this.Controls.Add(this.lblAlgorithm);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblShares);
            this.Controls.Add(this.lblTotalHashrate);
            this.Controls.Add(this.lblMinername);
            this.Name = "Home";
            this.Text = "Home";
            this.Load += new System.EventHandler(this.Home_Load);
            this.pnlStage1.ResumeLayout(false);
            this.pnlStage1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlStage2.ResumeLayout(false);
            this.pnlStage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctHeroImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctMiningState)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMinerState;
        private System.Windows.Forms.Button btnStartMining;
        private System.Windows.Forms.Label lblShares;
        private System.Windows.Forms.Label lblTotalHashrate;
        private System.Windows.Forms.Label lblMinername;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblAlgorithm;
        private System.Windows.Forms.Label lblPool;
        private System.Windows.Forms.Label lblWallet;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.Label lblSliderLocation1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblSliderLocation2;
        private System.Windows.Forms.TextBox txtWorkerId;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel pnlStage1;
        private System.Windows.Forms.Panel pnlStage2;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.LinkLabel linkLabel3;
        private System.Windows.Forms.PictureBox pctMiningState;
        private System.Windows.Forms.PictureBox pctHeroImage;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox pictureBox6;
    }
}